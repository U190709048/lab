import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };

		printBoard(board);

		int i =0;
		while(i!=9){
			System.out.print("Player 1 enter row number:");
			int row = reader.nextInt();
			System.out.print("Player 1 enter column number:");
			int col = reader.nextInt();
			while(row>3 || col>3){
				System.out.println("Please enter a valid number");
				System.out.print("Player 1 enter row number:");
				row = reader.nextInt();
				System.out.print("Player 1 enter column number:");
				col = reader.nextInt();
			}
			if(board[0][0]==board[0][1]  && board[0][1]==board[0][2]){
				System.out.println("Winner is player 1");
				break;

			}

			board[row - 1][col - 1] = 'X';
			printBoard(board);
			i++;

			System.out.print("Player 2 enter row number:");
			row = reader.nextInt();
			System.out.print("Player 2 enter column number:");
			col = reader.nextInt();
			while(row>3 || col>3){
				System.out.println("Please enter a valid number");
				System.out.print("Player 2 enter row number:");
				row = reader.nextInt();
				System.out.print("Player 2 enter column number:");
			}
			board[row - 1][col - 1] = 'O';
			printBoard(board);
			i++;

		}



	}
	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");

		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}


}